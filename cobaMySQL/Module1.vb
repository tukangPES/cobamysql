﻿Option Strict On

Imports MySql.Data


Module Module1

    Sub Main()
        Dim aConnection As MySqlClient.MySqlConnection
        aConnection = New MySqlClient.MySqlConnection("server=localhost;pwd=lookup;uid=root;database=cdcol;")

        aConnection.Open()

        Dim aCommand As IDbCommand
        aCommand = aConnection.CreateCommand()
        aCommand.CommandText = "coba"
        aCommand.CommandType = CommandType.StoredProcedure

        Dim aDataset As New DataSet
        Dim aDataAdapter As New MySqlClient.MySqlDataAdapter(CType(aCommand, MySqlClient.MySqlCommand))

        'Dim aParameter As MySqlClient.MySqlParameter
        'aParameter = New MySqlClient.MySqlParameter
        'aParameter.ParameterName = "abc"
        'aParameter.Direction = ParameterDirection.Output
        'aParameter.Size = 200
        'aParameter.Value = ""

        'aCommand.Parameters.Add(aParameter)


        aDataAdapter.Fill(aDataset)
        For Each aDataColumn As DataColumn In aDataset.Tables(0).Columns
            For Each aDatarow As DataRow In aDataset.Tables(0).Rows
                Console.Write(aDataColumn.ColumnName & "=" & CStr(aDatarow.Item(aDataColumn.ColumnName)) & " ,")
            Next
            Console.WriteLine()
        Next

        Console.ReadKey()


        aConnection.Close()
    End Sub

End Module
